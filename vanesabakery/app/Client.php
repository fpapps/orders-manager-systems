<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
	use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
			'nombre',
			'apellido',
			'telefono',
			'celular',
			'address',
			'email'
    ];

	protected $table = 'clients';
	
	protected $guarded = ['id'];

	protected $dates = ['deleted_at'];

	protected $appends = ['fullname'];

	/**
	 * Get the fullname for the client.
	 *
	 * @return bool
	 */
	public function getFullnameAttribute()
	{
		return $this->attributes['nombre'].' '.$this->attributes['apellido'];
	}

  public static function clientOptions()
  {
    return Client::all()
      ->map(function($client){
        return ['id' => $client->id, 'label' => $client->nombre];
      })->all();
  }
  
	public static function formFields($client){

		return [
			[
				'name' => 'nombre',
				'type' => 'text',
				'label' => 'Nombre',
				'value' => $client->nombre,
				'required' => true
			],
			[
				'name' => 'apellido',
				'type' => 'text',
				'label' => 'Apellido',
				'value' => $client->apellido,
				'required' => true
			],
			[
				'name' => 'telefono',
				'type' => 'text',
				'label' => 'Tel&eacute;fono',
				'value' => $client->telefono,
				'required' => true
			],
			[
				'name' => 'celular',
				'type' => 'text',
				'label' => 'Celular',
				'value' => $client->celular,
				'required' => true
			],
			[
				'name' => 'direccion',
				'type' => 'text',
				'label' => 'Direcci&oacute;n',
				'value' => $client->address,
				'required' => true
			],
			[
				'name' => 'email',
				'type' => 'text',
				'label' => 'Correo',
				'value' => $client->email,
				'required' => true
			],

		];
	}
}
