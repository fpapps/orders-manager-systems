<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ClientForm;
use App\Client;

class ClientController extends Controller
{
	public function __construct()
  {
       //$this->middleware('jwt.auth', ['except' => ['login']]);
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		    $clients = Client::paginate(5);
        return response()->json(['clients' => $clients]);
    }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function all_clients()
  {
    //
    $clients = Client::all();
    return response()->json(['clients' => $clients]);
  }


  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function search(Request $request)
  {
    $column = $request->input('column');
    $direction = $request->input('direction');
    $likeCriteria = $request->input('term') === '' ? '%' : $request->input('term');
    if($column === ''){
      $column = 'nombre';
      $direction = 'asc';
    }
    $searchField = ['nombre', 'telefono', 'email', 'celular'];
    $clients = Client::where(function($query) use ($searchField, $likeCriteria) {
      collect($searchField)->each(function($field) use ($query, $likeCriteria) {
        $query->orWhere($field, 'like', '%'.$likeCriteria.'%');
      });
    })->orderBy($column, $direction)
      ->paginate(5);

    return response()->json(['clients' => $clients]);
  }

	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientForm $request)
    {
        //
		    if($request->ajax())
		    {
			      $client = new Client($request->all());
			      $client->save();
            return response()->json(['message' => 'Client saved', 'id' => $client->id]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $client = Client::find($id);
        return response()->json(['client' => $client]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClientForm $request, $id)
    {
        //
        if($request->ajax())
        {
            $client = Client::find($id);
            $client->fill($request->all());
            $client->save();
            return response()->json(['message' => 'Client updated']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		    $user = Client::find($id);
        $user->delete();
        return response()->json(['message' => 'Client eliminado']);
    }
}
