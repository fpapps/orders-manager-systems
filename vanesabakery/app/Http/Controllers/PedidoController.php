<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PedidoForm;
use App\Pedido;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class PedidoController extends Controller
{
	public function __construct()
	{
       $this->middleware('jwt.auth', ['except' => []]);
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$pedidos = Pedido::paginate(20);
        return response()->json(['pedidos' => $pedidos]);
    }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function getPedidos($status = 'nuevo')
  {
    //
    $pedidos = Pedido::where(function($query) use ($status){
      $query->where('status', '=', $status);
    })->orderBy('fecha_entrega', 'asc')
    ->paginate(20);
    return response()->json(['pedidos' => $pedidos]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function search(Request $request)
  {
    $column = $request->input('column');
    $direction = $request->input('direction');
    $likeCriteria = $request->input('term') === '' ? '%' : $request->input('term');
    if($column === ''){
      $column = 'client';
      $direction = 'asc';
    }
    $searchField = ['client'];
    $pedidos = Pedido::where(function($query) use ($searchField, $likeCriteria) {
      collect($searchField)->each(function($field) use ($query, $likeCriteria) {
        $query->orWhere($field, 'like', '%'.$likeCriteria.'%');
      });
    })->orderBy($column, $direction)
      ->paginate(20);

    return response()->json(['pedidos' => $pedidos]);
  }

  
    //
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PedidoForm $request)
    {
        //
		if($request->ajax())
		{
            $user = JWTAuth::parseToken()->authenticate();
		    $pedido = new Pedido($request->all());
            $date = date_create($pedido->fecha_entrega);
            $pedido->fecha_entrega = date_format($date, 'Y-m-d H:i:s A');
            $pedido->user_id = $user->id;
            $pedido->modify_user = $user->id;
            $pedido->save();
            $this->_uploadFoto($pedido, $request->input('foto'), 'foto');
            return response()->json(['message' => 'Pedido saved', 'id' => $pedido->id]);
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $pedido = Pedido::find($id);
        return response()->json(['pedido' => $pedido]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if($request->ajax())
        {
            $pedido = Pedido::find($id);
            $pedido->fill($request->all());
            $date = date_create($pedido->fecha_entrega);
            $pedido->fecha_entrega = date_format($date, 'Y-m-d H:i:s A');
            $pedido->modify_user = JWTAuth::parseToken()->authenticate()->id;
            $pedido->save();
            return response()->json(['message' => 'Pedido updated']);
        }
    }

  private function _uploadFoto($pedido, $file, $fieldName)
  {
    if ($file && !strpos($file, '.jpg')) {
      $name = 'pedido' . time() . '.jpg';
      $path = public_path() . '/images/';
      $foto = '/images/'.$name;
      $this->base64_to_jpeg($file, $path.$name);
      $pedido->$fieldName = $foto;
      $pedido->save();

    }
  }

  /**
   * Return JPG image file
   * base on 64 string
   *
   * @private
   */
  private function base64_to_jpeg($base64_string, $output_file)
  {
    $ifp = fopen($output_file, "wb");

    $data = explode(',', $base64_string);

    fwrite($ifp, base64_decode($data[1]));
    fclose($ifp);

    return $output_file;
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		    $pedido = Pedido::find($id);
        $pedido->delete();
        return response()->json(['message' => 'Pedido eliminado']);
    }
}
