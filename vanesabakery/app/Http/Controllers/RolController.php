<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RolForm;
use App\Rol;
class RolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		    $roles = Rol::paginate(5);
        return response()->json(['roles' => $roles]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function roles_all()
    {
      //
      $roles = Rol::all();
      return response()->json(['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RolForm $request)
    {
        //
		    if($request->ajax())
        {
			    $rol = new Rol($request->all());
			    $rol->save();
          return response()->json(['message' => 'Rol saved']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		$rol = Rol::find($id);
		return response()->json(['rol' => $rol]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RolForm $request, $id)
    {
        //
		if($request->ajax())
		{
			$rol = Rol::find($id);
			$rol->fill($request->all());
			$rol->save();
            return response()->json(['message' => 'Rol updated']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$rol = Rol::find($id);
		$rol->delete();
        return response()->json(['message' => 'Rol eliminado']);
    }
}
