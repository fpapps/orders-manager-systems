<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UserForm;
use App\Http\Requests\UpdaterUserForm;
use App\Http\Requests\UploadUserCedula;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
	  public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['login', 'create', 'userlist', 'edit']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		    $users = User::paginate(5);
        return response()->json(['users' => $users]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $column = $request->input('column');
        $direction = $request->input('direction');
        $likeCriteria = $request->input('term') === '' ? '%' : $request->input('term');
        if($column === ''){
            $column = 'nombre';
            $direction = 'asc';
        }
        $searchField = ['nombre', 'telefono', 'email', 'cedula'];
        $users = User::where(function($query) use ($searchField, $likeCriteria) {
                collect($searchField)->each(function($field) use ($query, $likeCriteria) {
                    $query->orWhere($field, 'like', '%'.$likeCriteria.'%');
                 });
        })->orderBy($column, $direction)
          ->paginate(5);

        return response()->json(['users' => $users]);
    }
    //
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserForm $request)
    {
        //
		if($request->ajax())
		{
            $user = new User($request->all());
            $user->password = bcrypt($request->input('password'));
			      $user->save();
            return response()->json(['message' => 'User saved', 'id' => $user->id]);
        }
    }

    /**
     * Upload the ID card foto of the client
     *
     * @param UploadUserCedula $request
     * @param  \Illuminiate\Http\Response
     */

    public function uploadCedula(UploadUserCedula $request, $id)
    {
        $user = User::find($id);
        
        $foto_cedula = '';
        if($request->file('foto_cedula'))
        {
            $image = $request->file('foto_cedula');
            $name = 'user-cedula'.time().'.'.$image->getClientOriginalExtension();
            $path = public_path().'/images/users/';
            $image->move($path, $name);
            $foto_cedula = '/images/users/'.$name;
        }
        $user->foto_cedula = $foto_cedula;
        $user->save();
        return response()->json(['message' => 'User image uploaded']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::find($id);
        return response()->json(['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdaterUserForm $request, $id)
    {
        //
        if($request->ajax())
        {
            $user = User::find($id);
            $user->fill($request->all());
            $user->password = bcrypt($request->input('password'));
            $user->save();
            return response()->json(['message' => 'User updated']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$user = User::find($id);
       $user->delete();
        return response()->json(['message' => 'Usuario eliminado']);
    }

    /**
     * to login by email use this $credentials = $request->only('email', 'password')
     * return $this->_login($credentials)
     **/
    public function login(Request $request)
    {
        //return $request->only('username', 'password');
        return $this->_loginByUserName($request);
    }

    public function authenticate()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }

    private function _loginByUserName(Request $request)
    {
        if (!$user = User::findByName($request->input('username'))) {
            return response()->json(['error' => 'Usuario y/o contraseña incorrecto'], 401);
        }
        $credentials = [
          'email' => $user->email,
          'password' => $request->input('password')
        ];
        $exp = time() + (60 * 60 * 60 * 60);
        if ($request->input('rememberme') === 1)
            $exp = time() + (7 * 24 * 60 * 60 * 60); // nextweek
        return $this->_login($credentials, $exp);
    }

    private function _login($credentials, $exp)
    {

        try {

            // si los datos de login no son correctos
            if (!$token = JWTAuth::attempt($credentials, ['exp' => $exp])) {
                return response()->json(['error' => 'Usuario y/o contraseña incorrecto'], 401);
            }
        } catch (JWTException $e) {
            // si no se puede crear el token
            return response()->json(['error' => 'No se puede crear la session.'], 500);
        }
        $user = JWTAuth::authenticate($token);
        if ($user->active === 0) {
            return response()->json(['error' => 'Usuario no esta activo'], 401);
        }
        // todo bien devuelve el token
        return response()->json(['token' => compact('token'), 'user' => $user]);
    }
}
