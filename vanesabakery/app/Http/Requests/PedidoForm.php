<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PedidoForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          "client_id"		    =>		"required",
          "tipo_peido"	=>		"required",
          "libra"		=>		"required",
          "forma"		=>		"required",
          "planta"		    =>		"required",
          "design"		    =>		"required",
          "color"		=>		"required",
          "relleno"		=>		"required",
          "sabor"	=>		"required",
          "fecha_entrega"	=>		"required",
            
        ];
    }
}
