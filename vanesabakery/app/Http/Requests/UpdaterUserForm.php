<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdaterUserForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "nombre"	=>		"required|between:5,60",
            "direccion"		=>		"required|between:5,255",
            "telefono"		    =>		"required",
            "celular"		    =>		"required",
            "rol_id"		=>		"required",
            "salario"		=>		"required",
            "cedula"		=>		"required|min:10",
            "fecha_inicio"	=>		"required",
            "hora_entrada"	=>		"required",
            "hora_salida"	=>		"required",
            "email"		    =>		"required|email",
        ];
    }
	
	public function messages()
    {
        return [
            
        ];
    }
}
