<?php
/**
 * Created by PhpStorm.
 * User: Fermin
 * Date: 7/14/2016
 * Time: 3:28 PM
 */
namespace App\Http\Requests;

use App\Http\Requests\Request;

class UploadUserCedula extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "foto_cedula"    =>      "image|required"
        ];
    }
}