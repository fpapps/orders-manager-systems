<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "username"		    =>		"required|unique:users",
            "nombre"	=>		"required",
            "direccion"		=>		"required|between:5,255",
			      "cedula"		=>		"required|min:10",
            "telefono"		    =>		"required",
            "celular"		    =>		"required",
            "rol_id"		=>		"required",
            "salario"		=>		"required",
            "fecha_inicio"	=>		"required",
            "hora_entrada"	=>		"required",
            "hora_salida"	=>		"required",
            "password"		=>		"required|min:6|confirmed",
            "email"		    =>		"required|unique:users|email"
        ];
    }
	
	public function messages()
    {
        return [
            'password.required' => 'El :attribute es requerido.',
            'password.min' => 'por favor el :attribute debe de tener más de 6.',
            'password.confirmed' => 'El :attribute no conside con la confirmacion.',
        ];
    }
}
