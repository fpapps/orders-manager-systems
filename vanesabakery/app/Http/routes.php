<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


    /*
     *  Dashboard Routes
     */
    Route::get('/', function () {
        return view('dashboard.dashboard');
    });

    /*
     *  Login Routes
     */
    Route::get('/login', function () {
        return view('login/login');
    });

    /**
    * Users front end routes
    *
    **/
    Route::group(['prefix' => 'users'], function(){
        Route::get('/', function () {
            return view('users.list');
        });

        Route::get('/{id}', function($id){
            return view('users.edit');
        })->where('id', '[0-9]+');

        Route::get('/create', function(){
            return view('users.create');
        });

    });


    /**
     * Clients front end routes
     *
     **/
    Route::group(['prefix' => 'clients'], function(){
        Route::get('/', function () {
            return view('clients.list');
        });
    
        Route::get('/{id}', function($id){
            return view('clients.edit');
        })->where('id', '[0-9]+');
    
        Route::get('/create', function(){
            return view('clients.create');
        });
    
    });

    /**
     * Pedidos front end routes
     *
     **/
    Route::group(['prefix' => 'pedidos'], function(){
        Route::get('/', function () {
            return view('pedidos.list');
        });

        Route::get('/{id}', function($id){
            return view('pedidos.edit');
        })->where('id', '[0-9]+');

        Route::get('/print/{id}', function($id){
            return view('pedidos.print');
        })->where('id', '[0-9]+');

        Route::get('/create', function(){
            return view('pedidos.create');
        });


    });

    /**
     * Api routes
     */

    Route::group(['prefix' => 'api', 'middleware' => ['cors']], function(){

        /*
         * users routes
         */
        Route::resource('users', 'UserController', ['except' => [
            'create', 'edit'
        ]]);
        Route::post('users/search', 'UserController@search');
        Route::post('users/uploadcedula/{users}', 'UserController@uploadCedula');
        Route::post("login", 'UserController@login');
        Route::get("authenticate", 'UserController@authenticate');

        /*
         * roles routes
         */
        Route::resource('roles', 'RolController', ['except' => [
                'create', 'edit'
            ]]);
        Route::get('roles_all', 'RolController@roles_all');

        /*
        * clients routes
        */
        Route::resource('clients', 'ClientController', ['except' => [
        'create', 'edit'
        ]]);
        Route::post('clients/search', 'ClientController@search');
        Route::get('all_clients', 'ClientController@all_clients');

        /*
       * pedidos routes
       */
        Route::resource('pedidos', 'PedidoController', ['except' => [
          'create', 'edit'
        ]]);
        Route::post('pedidos/search', 'PedidoController@search');
        Route::get('pedido_by_status/{status}', 'PedidoController@getPedidos');

    });