<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Client;

class Pedido extends Model
{
	use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  protected $fillable = [
		'client_id',
		'tipo_peido',
		'libra',
		'forma',
		'planta',
		'design',
		'foto',
		'color',
		'relleno',
		'sabor',
		'cupcakes',
    'status',
		'fecha_entrega',
		'comentario',
		'cupcake_cantidad',
		'cupcake_relleno',
		'cupcake_color',
		'cupcake_design',
		'cupcake_sabor',
		'cupcake_comentario'
	];

	protected $table = 'pedidos';
	
	protected $guarded = ['id'];

  protected $dates = ['deleted_at'];

	protected $appends = ['client', 'fechaentrega2'];

	/**
	 * Get the fullname for the client.
	 *
	 * @return bool
	 */
	public function getClientAttribute()
	{
		$client = Client::find($this->attributes['client_id']);
		if(!$client)
		{
			$this->cancelarPedido();
		}
		return $client ? $client->fullname : 'Client deleted';
	}

	public function getFechaentrega2Attribute()
	{
		setlocale(LC_ALL,"es_ES");
		$date = date_create($this->attributes['fecha_entrega']);
		return date_format($date, 'F j, Y H:i:s A');
	}

	public function cancelarPedido()
	{
		$this->status = 'cancelado';
		$this->save();
	}
	
	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	private static function getFoto($pedido){
		return empty($pedido->foto) ? '/images/pedidos/no-image.jpg' : $pedido->foto;
	}

	public static function formFields($pedido){

		return [
			[
				'name' => 'client_id',
				'type' => 'select',
				'label' => 'Selecione el Cliente',
				'selected' => $pedido->client_id,
				'options' => Client::clientOptions()
			],
			[
				'name' => 'tipo_peido',
				'type' => 'select',
				'label' => 'Tipo de pedido',
				'selected' => $pedido->tipo_peido,
				'options' => [
					['id' => 'Delivery', 'label' => 'Delivery'],
					['id' => 'Take out', 'label' => 'Take out'],
				]
			],
			[
				'name' => 'libra',
				'type' => 'text',
				'label' => 'Libra',
				'value' => $pedido->libra,
				'required' => true
			],
			[
				'name' => 'forma',
				'type' => 'select',
				'label' => 'Forma',
				'selected' => $pedido->forma,
				'options' => [
					['id' => 'Redonda', 'label' => 'Redonda'],
					['id' => 'Cuadrada', 'label' => 'Cuadrada'],
				]
			],
			[
				'name' => 'planta',
				'type' => 'text',
				'label' => 'Planta',
				'value' => $pedido->planta,
				'required' => true
			],
			[
				'name' => 'design',
				'type' => 'text',
				'label' => 'Design',
				'value' => $pedido->design,
				'required' => true
			],
			[
				'name' => 'foto',
				'type' => 'image',
				'label' => 'Foto del bizcocho',
				'src' => self::getFoto($pedido),
			],
			[
				'name' => 'color',
				'type' => 'text',
				'label' => 'Color',
				'value' => $pedido->color,
				'required' => true
			],
			[
				'name' => 'relleno',
				'type' => 'text',
				'label' => 'Relleno',
				'value' => $pedido->relleno,
				'required' => true
			],
			[
				'name' => 'sabor',
				'type' => 'text',
				'label' => 'Sabor',
				'value' => $pedido->sabor,
				'required' => true
			],
			[
				'name' => 'cupcakes',
				'type' => 'text',
				'label' => 'Cup cakes',
				'value' => $pedido->cupcakes,
				'required' => true
			],
			[
				'name' => 'status',
				'type' => 'select',
				'label' => 'Status',
				'selected' => $pedido->status,
				'options' => [
					['id' => 'nuevo', 'label' => 'Nuevo'],
					['id' => 'preparacion', 'label' => 'Preparacion'],
					['id' => 'listo', 'label' => 'Listo'],
					['id' => 'entregado', 'label' => 'Entregado'],
					['id' => 'cancelado', 'label' => 'Cancelado'],
				]
			],

		];
	}
}
