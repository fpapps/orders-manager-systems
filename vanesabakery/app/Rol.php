<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rol extends Model
{
	use SoftDeletes;
    //
	protected $table = 'roles';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'nombre',
		'description',
    ];
	
	protected $guarded = ['id'];

    protected $dates = ['deleted_at'];
	
	public static function rolesOptions()
	{
		return Rol::all()
			->map(function($rol){
				return ['id' => $rol->id, 'label' => $rol->nombre];
			})->all();
	}
}
