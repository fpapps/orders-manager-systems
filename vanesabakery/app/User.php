<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Rol;

class User extends Authenticatable
{
	use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
		'nombre',
		'cedula',
		'telefono',
		'celular',
		'direccion',
		'foto_cedula',
		'email', 
		'rol_id',
		'fecha_inicio',
		'hora_entrada',
		'hora_salida',
		'salario'
    ];

	protected $table = 'users';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	protected $guarded = ['id'];

	protected $dates = ['deleted_at'];

	public static function findByName($name)
	{
		return self::where('username', $name)->first();
	}
	
	public function rol()
	{
		return $this->belongsTo(Rol::class);
	}

	private static function getFotoCedula($user){
		return empty($user->foto_cedula) ? '/assets/images/no-image.png' : $user->foto_cedula;
	}


	public static function formFields($user){

		return [
			[
				'name' => 'username',
				'type' => 'text',
				'label' => 'Nombre de usuario',
				'value' => $user->username,
				'required' => true
			],
			[
				'name' => 'nombre',
				'type' => 'text',
				'label' => 'Nombre',
				'value' => $user->nombre,
				'required' => true
			],
			[
				'name' => 'cedula',
				'type' => 'text',
				'label' => 'Cedula',
				'value' => $user->cedula,
				'required' => true
			],
			[
				'name' => 'telefono',
				'type' => 'text',
				'label' => 'Telefono',
				'value' => $user->telefono,
				'required' => true
			],
			[
				'name' => 'celular',
				'type' => 'text',
				'label' => 'Celular',
				'value' => $user->celular,
				'required' => true
			],
			[
				'name' => 'direccion',
				'type' => 'text',
				'label' => 'Dirección',
				'value' => $user->direccion,
				'required' => true
			],
			[
				'name' => 'email',
				'type' => 'text',
				'label' => 'Email',
				'value' => $user->email,
				'required' => true
			],
			[
				'name' => 'fecha_inicio',
				'type' => 'text',
				'label' => 'Fecha Inicio',
				'value' => $user->fecha_inicio,
				'required' => true
			],
			[
				'name' => 'hora_entrada',
				'type' => 'text',
				'label' => 'Hora Entrada',
				'value' => $user->hora_entrada,
				'required' => true
			],
			[
				'name' => 'hora_salida',
				'type' => 'text',
				'label' => 'Hora Salida',
				'value' => $user->hora_salida,
				'required' => true
			],
			[
				'name' => 'salario',
				'type' => 'text',
				'label' => 'Salario',
				'value' => $user->salario,
				'required' => true
			],
			[
				'name' => 'rol_id',
				'type' => 'select',
				'label' => 'Roles',
				'selected' => $user->rol_id,
				'options' => Rol::rolesOptions()
			],
			[
				'name' => 'password',
				'type' => 'password',
				'label' => 'Password',
				'value' => $user->password,
				'required' => true
			],
			[
				'name' => 'password_confirmation',
				'type' => 'password',
				'label' => 'Confirmal el password',
				'value' => $user->password_confirmation,
				'required' => true
			],
			[
				'name' => 'foto_cedula',
				'type' => 'image',
				'label' => 'Foto de Cedula',
				'src' => self::getFotoCedula($user)
			],

		];
	}
}
