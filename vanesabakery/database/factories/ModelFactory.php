<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->email,
        'nombre' => $faker->name,
        'cedula' => 9999999999,
        'telefono' => $faker->phoneNumber,
        'celular' => $faker->phoneNumber,
        'direccion' => $faker->address,
        'foto_cedula' => $faker->imageUrl($width = 640, $height = 480, $category = null, $randomize = true, $word = null, $gray = false),
        'email' => $faker->safeEmail,
		'rol_id' => $faker->randomDigit,
		'fecha_inicio' => $faker->randomDigit,
		'hora_entrada' => $faker->time($format = 'H:i:s', $max = 'now'),
		'hora_salida' => $faker->time($format = 'H:i:s', $max = 'now'),
		'salario' => $faker->numberBetween(10000,15000),
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Rol::class, function (Faker\Generator $faker) {
    return [
        'nombre' => str_random(10),
        'description' => $faker->text(30),
    ];
});

$factory->define(App\Client::class, function (Faker\Generator $faker) {
	return [
		'nombre' => str_random(10),
		'apellido' => $faker->text(30),
		'telefono' => str_random(10),
		'celular' => str_random(10),
		'email' => str_random(10),
		'address' => str_random(10),
	];
});

$factory->define(App\Pedidos::class, function (Faker\Generator $faker) {
	return [
	];
});
