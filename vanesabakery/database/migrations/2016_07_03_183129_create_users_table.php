<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
			$table->integer('rol_id')->unsigned();
			$table->string('nombre');
			$table->string('cedula');
			$table->string('telefono');
			$table->string('celular');
			$table->string('direccion');
			$table->string('fecha_inicio');
			$table->float('salario');
			$table->time('hora_entrada');
			$table->time('hora_salida');
			$table->string('foto_cedula');
            $table->rememberToken();
            $table->timestamps();
			$table->softDeletes();
			
			$table->foreign('rol_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
