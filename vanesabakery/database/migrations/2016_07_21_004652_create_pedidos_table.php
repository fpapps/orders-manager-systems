<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();;
            $table->integer('user_id')->unsigned();;
            $table->integer('modify_user')->unsigned();;
            $table->string('tipo_peido');
            $table->string('libra');
            $table->string('forma');
            $table->integer('planta');
            $table->string('design');
            $table->string('foto');
            $table->string('color');
            $table->string('relleno');
            $table->string('sabor');
            $table->string('cupcakes');
            $table->enum('status', ['nuevo', 'preparando', 'listo', 'entregado', 'cancelado'])->default('nuevo');
            $table->dateTime('fecha_entrega');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('modify_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('pedidos');
    }
}
