<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtrasColumnsToPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            //
            $table->string('comentario')->default("");
            $table->string('cupcake_cantidad')->default("");
            $table->string('cupcake_relleno')->default("");
            $table->string('cupcake_color')->default("");
            $table->string('cupcake_design')->default("");
            $table->string('cupcake_sabor')->default("");
            $table->string('cupcake_comentario')->default("");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            //
            $table->dropColumn(['comentario', 
			'cupcake_cantidad',
			'cupcake_relleno',
			'cupcake_color',
			'cupcake_design',
			'cupcake_sabor',
			'cupcake_comentario',
			]);
        });
    }
}
