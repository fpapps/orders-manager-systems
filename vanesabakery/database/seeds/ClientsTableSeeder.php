<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('clients')->insert([
          'nombre' => 'juan',
          'apellido' => 'lopez',
          'telefono' => '12345678',
          'celular' => '12345678',
          'address' => 'test address',
          'address' => 'testjuan@vanessabakery.com',
        ]);
    }
}
