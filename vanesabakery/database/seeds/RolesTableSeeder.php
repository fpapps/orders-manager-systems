<?php
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
            ['id' => 1, 'nombre' => "'Administrador'", 'description' => "''"],
            ['id' => 2, 'nombre' => "'Servicio al Client'", 'description' => "''"],
            ['id' => 3, 'nombre' => "'Repostero'", 'description' => "''"],
         
        ]);
    }
}