<?php
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'username' => 'admin',
            'nombre' => 'admin',
            'cedula' => '12345678',
            'telefono' => '12345678',
            'celular' => '12345678',
            'direccion' => '12345678',
            'foto_cedula' => '',
            'email' => 'admin@vanessabakery.com',
            'rol_id' => 1,
            'fecha_inicio' => '2016-07-13',
            'hora_entrada' => '8:00',
            'hora_salida' => '8:00',
            'salario' => 1500,
            'password' => bcrypt('welc0me')
        ]);
    }
}