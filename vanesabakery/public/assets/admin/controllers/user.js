/**
 * Created by omar on 7/14/2016.
 */
(function($){
    'use strict';
    var self = {};
    self.user = {};
    self.validactions = {};
    self.files = {};
    
    function clearError(){
        for(var prop in self.user)
        {
            $("[name='"+prop+"']").parent().removeClass('has-error');

        }
        $('.help-block').remove();
    }

    function showError(){
        for(var prop in self.validactions)
        {
            var formGroup = $("[name='"+prop+"']").parent();
            formGroup.addClass('has-error');
            if(prop == 'password')
            {
                formGroup.append('<p class="help-block">'+self.validactions[prop]+'</p>');
            }

        }
    }

    function showSuccess(){
        console.log('success ...');
    }

    function uploadImage(files, id){
        var xhr = XMLHttpRequest = new XMLHttpRequest();

        xhr.onreadystatechange = function(){
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    JSON.parse(xhr.response);
                } else {
                   JSON.parse(xhr.response);
                }
            }
        };

        xhr.upload.onprogress = function(event){
            this.progress = Math.round(event.loaded / event.total * 100);
            console.log(this.progress);
        };
        var formdata = new FormData();

        if(files['foto_cedula'])
            formdata.append('foto_cedula', files['foto_cedula'], files['foto_cedula'].name);

        xhr.open('POST', '/api/users/uploadcedula/' + id, true);
        xhr.send(formdata);
    }

    function getUrl(){
        return gobla.update ? "/api/users/"+gobla.user.id : "/api/users/";
    }

    function update(url){
        $.ajax({
            url : url,
            data : $( "#user" ).serialize(),
            type : 'PATCH',
            contentType : 'application/json',
            xhr: function() {
                return window.XMLHttpRequest == null || new window.XMLHttpRequest().addEventListener == null
                    ? new window.ActiveXObject("Microsoft.XMLHTTP")
                    : $.ajaxSettings.xhr();
            },
            success : function(data){
                clearError();
                showSuccess();
                uploadImage(self.files, data.id);
            },
            error : function(){
                self.validactions = data.responseJSON;
                clearError();
            }
        });
    }

    function save(url){
        $.post(url, $( "#user" ).serialize() ,function(data, status){
            clearError();
            showSuccess();
            uploadImage(self.files, data.id);
        }).fail(function(data) {
            self.validactions = data.responseJSON;
            clearError();
            showError();
        });
    }

    $('#save').click(function(){
        self.user = $( "#user" ).serializeObject();
        var url = getUrl();
        if(gobla.update)
            update(url);
        else
            save();

        return false;
    });



    $('#foto_cedula').on('change', function(event){
        self.files['foto_cedula'] = event.target.files[0];
        var div = $(this).parent();
        div.find('.img-responsive').attr('src', URL.createObjectURL(event.target.files[0]));
    });
    

}(jQuery));