angular.module('invertapp')
  .controller('CreateClientController', function ($scope, $InvertAppClientService) {
    $scope.clientData = {
    };
    $scope.createClient = function () {
      $InvertAppClientService.createClient({
        data: $scope.clientData,
        successCallback: function (result) {
          $scope.isSuccess = result.data.message === 'Client saved';
          $scope.clientData['error'] = undefined;
        },
        errorCallback: function (error) {
          $scope.isSuccess = false;
          $scope.clientData['error'] = error.data;
        }
      });
    };

  });