angular.module('invertapp')
  .controller('EditClientController', function ($scope, $window, $InvertAppClientService) {
    $scope.isRequestInProgress = true;

    $scope.saveClient = function () {
      $InvertAppClientService.saveClient({
        data: $scope.clientData,
        successCallback: function (result) {
          console.log('sucess client result  :: ', result);
          $scope.isSuccess = result.data.message === 'Client updated';
          $scope.clientData['error'] = undefined;
        },
        errorCallback: function (error) {
          $scope.isSuccess = false;
          $scope.clientData['error'] = error.data;
        }
      });
    };

    var loadClient = function () {
      var pathname = $window.location.pathname.split('/');
      var clientId = pathname[pathname.length - 1];

      $InvertAppClientService.getClient({
        client_id: clientId,
        successCallback: function (result) {
          $scope.clientData = result.data.client;
          $scope.isRequestInProgress = false;
        }
      });
    };
    loadClient();
  });