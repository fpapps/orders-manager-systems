angular.module('invertapp')
  .controller('ListClientController', function ($scope, $InvertAppClientService) {
    var criteria = "";
    $scope.currentPage = 1;
    $scope.header = {
      nombre: {
        asc: true
      }
    };

    $scope.indexSortBy = 0;
    $scope.sortDirection = 'asc';

    var updatePagination = function updatePagination(queryResult) {
      $scope.clients = queryResult.data.clients.data;
      $scope.pages = new Array(queryResult.data.clients.last_page);
    };

    var loadClients = function loadClients() {
      $InvertAppClientService.getClients({
        successCallback: updatePagination
      });
    };

    var updateHeaderClass = function updateHeaderClass(sortByColumnName) {
      var header = {};
      header[sortByColumnName] = {asc: true};
      if ($scope.header[sortByColumnName]) {
        header[sortByColumnName].asc = !$scope.header[sortByColumnName].asc;
        $scope.header = header;
        return;
      }
      $scope.header = header;
    };

    var getColumn = function getColumn() {
      var key = Object.keys($scope.header)[0];

      return {
        name: key,
        direction: $scope.header[key].asc ? 'asc' : 'desc'
      };
    };

    var getCriteria = function getCriteria(currentPage) {
      $scope.currentPage = currentPage;
      var column = getColumn();

      return {
        column: column.name,
        direction: column.direction,
        page: currentPage,
        term: criteria || ''
      };
    };

    var httpSearchRequest = function httpSearchRequest(currentPage) {
      $InvertAppClientService.searchClients({
        data: getCriteria(currentPage),
        successCallback: updatePagination
      });
    };

    $scope.sortBy = function (sortByColumnName) {
      updateHeaderClass(sortByColumnName);
      httpSearchRequest($scope.currentPage);
    };

    $scope.searchClient = function searchClient(searchCriteria) {
      criteria = searchCriteria;
      httpSearchRequest(1);
    };

    $scope.setPage = function setPage(pageNumber) {
      httpSearchRequest(pageNumber);
    };

    $scope.deleteClient = function deleteClient(client) {
      $InvertAppClientService.removeClient({
        data: {
          id: client.id
        },
        successCallback: function () {
          jQuery('#modal').modal('hide');
          httpSearchRequest($scope.currentPage);
        }
      });
    };

    loadClients();
  });
