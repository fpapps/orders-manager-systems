angular.module('invertapp')
  .controller('DashboardController', function ($scope, $InvertAppAuthService) {
    $scope.userData = {};

    var loadUser = function user() {
      $InvertAppAuthService.getUser({
        successCallback: function successCallback(result) {
          console.log('RESULT_DATA  ::  ', result.data);
          $scope.userData = result.data.user;
        }
      })
    };

    loadUser();
  });
