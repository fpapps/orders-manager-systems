angular.module('invertapp')
  .controller('MenuController', function ($scope, $InvertAppAuthService) {
    $scope.auth = {
    };
    
    $scope.canSeeClients = function canSeeClients(){
       return auth.rol_id == 1 || auth.rol_id == 2;
    };

    var loadAuth = function loadAuth(){
      $InvertAppAuthService.getUser({
        successCallback: function (result) {
          $scope.auth = result.data.user;
          
        },
      });
    };

    loadAuth();

  });
