angular.module('invertapp')
  .controller('CreatePedidoController', function ($scope, $InvertAppPedidoService, $InvertAppClientService, $window) {
    $scope.pedidoData = {
      'cupcake_cantidad' : '',
      'cupcake_relleno' : '',
      'cupcake_color' : '',
      'cupcake_design' : '',
      'cupcake_sabor' : '',
      'cupcake_comentario' : '',
      'comentario' : '',
    };
    $scope.createPedido = function () {
      $InvertAppPedidoService.createPedido({
        data: $scope.pedidoData,
        successCallback: function (result) {
          $scope.isSuccess = result.data.message === 'Pedido saved';
          $scope.pedidoData['error'] = undefined;
          $window.scrollTo(0, 0);
        },
        errorCallback: function (error) {
          $scope.isSuccess = false;
          $scope.pedidoData['error'] = error.data;
        }
      });
    };

    var loadClients = function loadClients() {
      $InvertAppClientService.getAllClients({
        successCallback: function (result) {
          $scope.pedidoClients = result.data.clients;
        }
      });
    };
    loadClients();
    
  });