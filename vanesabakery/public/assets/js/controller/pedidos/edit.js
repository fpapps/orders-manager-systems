angular.module('invertapp')
  .controller('EditPedidoController', function ($scope, $InvertAppPedidoService, $window, $InvertAppClientService) {
    $scope.isRequestInProgress = true;

    $scope.savePedido = function () {
      $InvertAppPedidoService.savePedido({
        data: $scope.pedidoData,
        successCallback: function (result) {
          $scope.isSuccess = result.data.message === 'Pedido updated';
          $scope.pedidoData['error'] = undefined;
          $window.scrollTo(0, 0);
        },
        errorCallback: function (error) {
          $scope.isSuccess = false;
          $scope.pedidoData['error'] = error.data;
        }
      });
    };

    var loadClients = function loadClients() {
      $InvertAppClientService.getAllClients({
        successCallback: function (result) {
          $scope.pedidoClients = result.data.clients;
        }
      });
    };
    

    var loadPedido = function () {
      var pathname = $window.location.pathname.split('/');
      var pedidoId = pathname[pathname.length - 1];

      $InvertAppPedidoService.getPedido({
        pedido_id: pedidoId,
        successCallback: function (result) {
          $scope.pedidoData = result.data.pedido;
          $scope.pedidoData.libra = parseInt($scope.pedidoData.libra);
          $scope.isRequestInProgress = false;
        }
      });
    };
    
    loadClients();
    loadPedido();
    
  });