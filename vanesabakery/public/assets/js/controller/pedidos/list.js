angular.module('invertapp')
  .controller('ListPedidoController', function ($scope, $InvertAppPedidoService) {
    var criteria = "";
    $scope.currentPage = 1;
    $scope.header = {
      client: {
        asc: true
      }
    };
    $scope.tap = {
      nuevo : true
    }
    $scope.status = "nuevo";

    $scope.indexSortBy = 0;
    $scope.sortDirection = 'asc';

    var updatePagination = function updatePagination(queryResult) {
      $scope.pedidos = queryResult.data.pedidos.data;
      $scope.pages = new Array(queryResult.data.pedidos.last_page);
    };

    var loadPedidos = function loadPedidos() {
      $InvertAppPedidoService.pedidoByStatus({
        status : $scope.status,
        successCallback: updatePagination
      });
    };

    var updateHeaderClass = function updateHeaderClass(sortByColumnName) {
      var header = {};
      header[sortByColumnName] = {asc: true};
      if ($scope.header[sortByColumnName]) {
        header[sortByColumnName].asc = !$scope.header[sortByColumnName].asc;
        $scope.header = header;
        return;
      }
      $scope.header = header;
    };

    var getColumn = function getColumn() {
      var key = Object.keys($scope.header)[0];

      return {
        name: key,
        direction: $scope.header[key].asc ? 'asc' : 'desc'
      };
    };

    var getCriteria = function getCriteria(currentPage) {
      $scope.currentPage = currentPage;
      var column = getColumn();

      return {
        column: column.name,
        direction: column.direction,
        page: currentPage,
        term: criteria || ''
      };
    };

    var httpSearchRequest = function httpSearchRequest(currentPage) {
      $InvertAppPedidoService.searchPedidos({
        data: getCriteria(currentPage),
        status: $scope.status,
        successCallback: updatePagination
      });
    };

    $scope.sortBy = function (sortByColumnName) {
      updateHeaderClass(sortByColumnName);
      httpSearchRequest($scope.currentPage);
    };

    $scope.searchPedido = function searchPedido(searchCriteria) {
      criteria = searchCriteria;
      httpSearchRequest(1);
    };

    $scope.setPage = function setPage(pageNumber) {
      httpSearchRequest(pageNumber);
    };

    $scope.deletePedido = function deletePedido(pedido) {
      $InvertAppPedidoService.removePedido({
        data: {
          id: pedido.id
        },
        successCallback: function () {
          jQuery('#modal').modal('hide');
          httpSearchRequest($scope.currentPage);
        }
      });
    };

    $scope.filterByStatus = function filterByStatus(status){
      $scope.tap = { }
      $scope.tap[status] = true;
      $scope.status = status;
      loadPedidos();
    };

    $scope.cancelarPedido = function cancelarPedido(pedido)
    {
      pedido.status = 'cancelado';
      $scope.savePedido(pedido);
    }

    $scope.savePedido = function (pedido) {
      $InvertAppPedidoService.savePedido({
        data: pedido,
        successCallback: function (result) {
          $scope.isSuccess = result.data.message === 'Pedido updated';
          loadPedidos();
        },
        errorCallback: function (error) {
          $scope.isSuccess = false;
          $scope.userData['error'] = error.data;
        }
      });
    };

    loadPedidos();
  });
