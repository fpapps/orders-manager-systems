angular.module('invertapp')
  .controller('CreateRolController', function ($scope, $InvertAppRolService) {
    $scope.rolData = {};
    $scope.createRol = function () {
      $InvertAppRolService.createRol({
        data: $scope.rolData,
        successCallback: function (result) {
          $scope.isSuccess = result.data.message === 'Rol saved';
          $scope.rolData['error'] = undefined;
        },
        errorCallback: function (error) {
          $scope.isSuccess = false;
          $scope.rolData['error'] = error.data;
        }
      });
    };

  });