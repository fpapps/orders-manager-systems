angular.module('invertapp')
  .controller('ListRolController', function ($scope, $InvertAppRolService) {
    var criteria = "";
    $scope.currentPage = 1;
    $scope.header = {
      name: {
        asc: true
      }
    };

    $scope.indexSortBy = 0;
    $scope.sortDirection = 'asc';

    var updatePagination = function updatePagination(queryResult) {
      $scope.roles = queryResult.data.roles.data;
      $scope.pages = new Array(queryResult.data.roles.last_page);
    };

    var loadRoles = function loadRoles() {
      $InvertAppRolService.getPaginate({
        successCallback: updatePagination
      });
    };

    var updateHeaderClass = function updateHeaderClass(sortByColumnName) {
      var header = {};
      header[sortByColumnName] = {asc: true};
      if ($scope.header[sortByColumnName]) {
        header[sortByColumnName].asc = !$scope.header[sortByColumnName].asc;
        $scope.header = header;
        return;
      }
      $scope.header = header;
    };

    var getColumn = function getColumn() {
      var key = Object.keys($scope.header)[0];

      return {
        name: key,
        direction: $scope.header[key].asc ? 'asc' : 'desc'
      };
    };

    var getCriteria = function getCriteria(currentPage) {
      $scope.currentPage = currentPage;
      var column = getColumn();

      return {
        column: column.name,
        direction: column.direction,
        page: currentPage,
        term: criteria || ''
      };
    };

    var httpSearchRequest = function httpSearchRequest(currentPage) {
      $InvertAppRolService.searchRoles({
        data: getCriteria(currentPage),
        successCallback: updatePagination
      });
    };

    $scope.sortBy = function (sortByColumnName) {
      updateHeaderClass(sortByColumnName);
      httpSearchRequest($scope.currentPage);
    };

    $scope.searchRol = function searchUser(searchCriteria) {
      criteria = searchCriteria;
      httpSearchRequest(1);
    };

    $scope.setPage = function setPage(pageNumber) {
      httpSearchRequest(pageNumber);
    };

    $scope.deleteRol = function deleteRol(user) {
      $InvertAppUserService.removeRol({
        data: {
          id: user.id
        },
        successCallback: function () {
          jQuery('#modal').modal('hide');
          httpSearchRequest($scope.currentPage);
        }
      });
    };

    loadRoles();
  });
