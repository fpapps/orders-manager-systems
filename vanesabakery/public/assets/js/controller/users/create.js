angular.module('invertapp')
  .controller('CreateUserController', function ($scope, $InvertAppUserService, $InvertAppRolService) {
    $scope.userData = {
      foto_cedula : '/assets/images/no-image.png'
    };
    $scope.createUser = function () {
      $InvertAppUserService.createUser({
        data: $scope.userData,
        successCallback: function (result) {
          $scope.isSuccess = result.data.message === 'User saved';
          $scope.userData['error'] = undefined;
        },
        errorCallback: function (error) {
          $scope.isSuccess = false;
          $scope.userData['error'] = error.data;
        }
      });
    };

    $scope.onChageFoto = function(foto){
      //console.log(foto.files);
      $scope.file = foto.files[0];
      $scope.userData.foto_cedula = URL.createObjectURL($scope.file);
    };

    $scope.selectFoto = function selectFoto(){
      $("#foto_cedula").click();
    };

    var loadRoles = function loadRoles() {
      $InvertAppRolService.getRoles({
        successCallback: function (result) {
          $scope.userRoles = result.data.roles;
        }
      });
    };

    loadRoles();
  });