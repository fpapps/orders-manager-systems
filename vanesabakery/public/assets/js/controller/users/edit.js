angular.module('invertapp')
  .controller('EditUserController', function ($scope, $window, $InvertAppUserService, $InvertAppRolService) {
    $scope.isRequestInProgress = true;

    $scope.saveUser = function () {
      $InvertAppUserService.saveUser({
        data: $scope.userData,
        successCallback: function (result) {
          console.log('sucess user result  :: ', result);
          $scope.isSuccess = result.data.message === 'User updated';
          $scope.userData['error'] = undefined;
        },
        errorCallback: function (error) {
          $scope.isSuccess = false;
          $scope.userData['error'] = error.data;
        }
      });
    };

    var loadRoles = function loadRoles() {
      $InvertAppRolService.getRoles({
        successCallback: function (result) {
          $scope.userRoles = result.data.roles;
          loadUser();
        }
      });
    };

    var loadUser = function () {
      var pathname = $window.location.pathname.split('/');
      var userId = pathname[pathname.length - 1];

      $InvertAppUserService.getUser({
        user_id: userId,
        successCallback: function (result) {
          $scope.userData = result.data.user;
          $scope.isRequestInProgress = false;
        }
      });
    };

    loadRoles();
  });