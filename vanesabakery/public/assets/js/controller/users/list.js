angular.module('invertapp')
  .controller('ListUserController', function ($scope, $InvertAppUserService) {
    var criteria = "";
    $scope.currentPage = 1;
    $scope.header = {
      nombre: {
        asc: true
      }
    };

    $scope.indexSortBy = 0;
    $scope.sortDirection = 'asc';

    var updatePagination = function updatePagination(queryResult) {
      $scope.users = queryResult.data.users.data;
      $scope.pages = new Array(queryResult.data.users.last_page);
    };

    var loadUsers = function loadUsers() {
      $InvertAppUserService.getUsers({
        successCallback: updatePagination
      });
    };

    var updateHeaderClass = function updateHeaderClass(sortByColumnName) {
      var header = {};
      header[sortByColumnName] = {asc: true};
      if ($scope.header[sortByColumnName]) {
        header[sortByColumnName].asc = !$scope.header[sortByColumnName].asc;
        $scope.header = header;
        return;
      }
      $scope.header = header;
    };

    var getColumn = function getColumn() {
      var key = Object.keys($scope.header)[0];

      return {
        name: key,
        direction: $scope.header[key].asc ? 'asc' : 'desc'
      };
    };

    var getCriteria = function getCriteria(currentPage) {
      $scope.currentPage = currentPage;
      var column = getColumn();

      return {
        column: column.name,
        direction: column.direction,
        page: currentPage,
        term: criteria || ''
      };
    };

    var httpSearchRequest = function httpSearchRequest(currentPage) {
      $InvertAppUserService.searchUsers({
        data: getCriteria(currentPage),
        successCallback: updatePagination
      });
    };

    $scope.sortBy = function (sortByColumnName) {
      updateHeaderClass(sortByColumnName);
      httpSearchRequest($scope.currentPage);
    };

    $scope.searchUser = function searchUser(searchCriteria) {
      criteria = searchCriteria;
      httpSearchRequest(1);
    };

    $scope.setPage = function setPage(pageNumber) {
      httpSearchRequest(pageNumber);
    };

    $scope.deleteUser = function deleteUser(user) {
      $InvertAppUserService.removeUser({
        data: {
          id: user.id
        },
        successCallback: function () {
          jQuery('#modal').modal('hide');
          httpSearchRequest($scope.currentPage);
        }
      });
    };

    loadUsers();
  });
