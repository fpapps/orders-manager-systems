angular.module('invertapp')
  .directive('clientForm', [function(){
    return{
      restrict: "E",
      require: ["invertAppTitle"],
      replace: true,
      scope: {
        isEdit: '=',
        isSuccess: "=",
        clientData: "=",
        saveClientFunction: '='

      },

      templateUrl: "/assets/template/clients/client-form.html"
    }
}]);
