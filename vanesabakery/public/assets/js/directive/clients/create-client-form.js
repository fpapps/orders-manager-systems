angular.module('invertapp')
  .directive('createClientForm', [function (){
    return{
      restrict: "AE",
      require: ["invertAppTitle"],
      scope:{
        isSuccess: '=',       //is-success
        clientData: '=',        //client-data
        saveClientFunction: '=', //save-client-function
      },
      replace: true,
      templateUrl: "/assets/template/clients/create-client-form.html"
    }
  }]);
