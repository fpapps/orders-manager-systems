angular.module("invertapp")
  .directive('deleteClientModal', function(){
    return {
      restrict: "E",
      replace: true,
      scope: {
        clientToBeDelete: "=",
        onRemoveFunction: '='
      },
      require: ['invertAppModal'],
      templateUrl: "/assets/template/clients/delete-client-modal.html"
    }
  });
