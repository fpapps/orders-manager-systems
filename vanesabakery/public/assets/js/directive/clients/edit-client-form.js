angular.module('invertapp')
  .directive('editClientForm', [function (){
    return{
      restrict: "AE",
      require: ["invertAppTitle"],
      scope:{
        isSuccess: '=',       //is-success
        isNotRequesting: '=', //is-not-requesting
        clientData: '=',        //client-data
        saveClientFunction: '=' //save-client-function
      },
      replace: true,
      templateUrl: "/assets/template/clients/edit-client-form.html"
    }
  }]);
