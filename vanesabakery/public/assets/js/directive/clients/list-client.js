angular.module('invertapp')
  .directive('listClient', [function(){
    return{
      restrict: "E",
      require: ["invertAppTitle", 'searchBox', 'invertAppModal'],
      replace: true,
      scope: {
        currentPage: '=', //current-page
        getPageNumber: '=', //get-page-number
        header: '=',   //header
        onPageClick: '=', //on-page-click
        pages: '=',       //pages
        searchCriteria: '=',
        searchClient: '=', //search-client
        sortByFunction: '=', //sort-by
        clients: '=', //clients,
        clientToBeDelete: '=' //client-to-be-delete
      },
      controller: function($scope) {
        $scope.setClient = function setClient(client) {
          $scope.clientToBeDelete = client;
        }
      },
      templateUrl: "/assets/template/clients/list-client.html"
    }
  }]);
