angular.module('invertapp')
  .directive('dashboard', [function () {
    return {
      restrict: "E",
      require: ['invertAppTitle'],
      scope: {
        user: '='
      },
      templateUrl: "/assets/template/dashboard/dashboard.html"
    }
  }]);
