angular.module("invertapp")
  .controller("InvertAppTitleController", function($scope){
    $scope.className = $scope.className ? "page-header " + $scope.className : 'page-header';
  })
  .directive('invertAppTitle', function(){
    return {
      restrict: "E",
      transclude: true,
      scope:{
        className: "@myClassName"
      },
      templateUrl: "/assets/template/general/invert-app-title.html"
    }
});
