angular.module("invertapp")
  .directive('myFile', function(){
    return {
      restrict: "A",
      scope:{
        myFile: '='
      },
      link: function(scope, element, attributes) {
        element.bind('change', function (event) {
          var reader = new FileReader();
          reader.onload = function (loadEvent) {
            scope.$apply(function () {
              scope.myFile = loadEvent.target.result;
            });
          };
          reader.readAsDataURL(event.target.files[0]);
        })
      }
    }
});
