angular.module('invertapp')
  .directive('login', [function () {
    return {
      restrict: "E",
      scope: {},
      controller: function ($scope, $InvertAppAuthService) {
        $scope.errors = [];
        $scope.login = function login() {
          localStorage.id_token = "";
          $scope.isLoading = true;
          $InvertAppAuthService.loginUser({
            data: {
              username: $scope.username,
              password: $scope.password,
              rememberme: $scope.rememberMe
            },
            successCallback: function (result) {
              localStorage.id_token = result.data.token.token;
              document.location.href = '/';
            },
            errorCallback: function (error) {
              $scope.error = error.data.error;
              console.log('ERROR   ', error);
              $scope.isLoading = false;
            }
          })
        }
      },
      templateUrl: "/assets/template/login/login.html"
    }
  }]);
