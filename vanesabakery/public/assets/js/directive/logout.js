angular.module('invertapp')
  .directive('logout', [function () {
    return {
      restrict: "E",
      scope: {},
      controller: function ($scope) {
        $scope.logout = function () {
          localStorage.id_token = undefined;
          document.location.href = "/login";
        };
      },
      templateUrl: "/assets/template/login/logout.html"
    }
  }]);