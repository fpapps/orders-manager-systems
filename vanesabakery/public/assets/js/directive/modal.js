angular.module("invertapp")
  .directive('invertAppModal', function(){
    return {
      restrict: "E",
      transclude: true,
      templateUrl: "/assets/template/general/modal.html"
    }
  });
