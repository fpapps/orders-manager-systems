angular.module('invertapp')
  .directive('createPedidoForm', [function (){
    return{
      restrict: "AE",
      require: ["invertAppTitle"],
      scope:{
        isSuccess: '=',       //is-success
        pedidoData: '=',        //pedido-data
        pedidoClients: '=',        //pedido-clients
        savePedidoFunction: '=' //save-pedido-function
      },
      replace: true,
      templateUrl: "/assets/template/pedidos/create-pedido-form.html"
    }
  }]);