angular.module('invertapp')
  .directive('editPedidoForm', [function (){
    return{
      restrict: "AE",
      require: ["invertAppTitle"],
      scope:{
        isSuccess: '=',       //is-success
        isNotRequesting: '=', //is-not-requesting
        pedidoData: '=',        //pedido-data
        pedidoClients: '=',       //pedido-clients
        savePedidoFunction: '=' //save-pedido-function
      },
      replace: true,
      controller: function($scope) {
        
      },
      templateUrl: "/assets/template/pedidos/edit-pedido-form.html"

    }
  }]);
