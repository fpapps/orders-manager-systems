angular.module('invertapp')
  .directive('listPedido', [function(){
    return{
      restrict: "E",
      require: ["invertAppTitle", 'searchBox', 'invertAppModal'],
      replace: true,
      scope: {
        currentPage: '=', //current-page
        getPageNumber: '=', //get-page-number
        header: '=',   //header
        onPageClick: '=', //on-page-click
        pages: '=',       //pages
        searchCriteria: '=',
        searchPedido: '=', //search-pedido
        sortByFunction: '=', //sort-by
        pedidos: '=', //pedidos
        pedidoSelected: '=',
        cancelarPedidoFunction: '=', // cancelar-pedido
      },
      controller: function($scope) {
        $scope.setPedido = function setPedido(pedido) {
          $scope.pedidoSelected = pedido;
        }
      },
      templateUrl: "/assets/template/pedidos/list-pedido.html"
    }
  }]);