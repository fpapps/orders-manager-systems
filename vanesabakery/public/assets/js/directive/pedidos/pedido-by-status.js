angular.module('invertapp')
  .directive('pedidoByStatus', [function(){
    return{
      restrict: "E",
      require: ["listPedido"],
      scope: {
        currentPage: '=', //current-page
        getPageNumber: '=', //get-page-number
        header: '=',   //header
        onPageClick: '=', //on-page-click
        pages: '=',       //pages
        searchCriteria: '=',
        searchPedido: '=', //search-pedido
        sortByFunction: '=', //sort-by
        pedidos: '=', //pedidos
        pedidoSelected: '='
      },
      controller: function($scope) {
        $scope.setPedido = function setPedido(pedido) {
          $scope.pedidoSelected = pedido;
        }
      },
      templateUrl: "/assets/template/pedidos/list-pedido.html"
    }
  }]);