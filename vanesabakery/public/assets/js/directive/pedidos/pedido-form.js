angular.module('invertapp')
  .directive('pedidoForm', [function(){
    return{
      restrict: "E",
      require: ["invertAppTitle"],
      replace: true,
      scope: {
        isEdit: '=',
        isSuccess: "=",
        pedidoData: "=",
        pedidoClients: "=",
        savePedidoFunction: '='
      },
      controller : function ($scope) {
        $('.datetime').datetimepicker({ format: 'YYYY-MM-D hh:mm:ss a'});
        $('.datetime').on('blur', function () {
          $scope.pedidoData.fecha_entrega = $('.datetime').val();
        });
      },
      templateUrl: "/assets/template/pedidos/pedido-form.html"
    }
  }]);