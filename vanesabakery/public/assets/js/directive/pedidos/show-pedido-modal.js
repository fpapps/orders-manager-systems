angular.module("invertapp")
  .directive('showPedidoModal', function(){
    return {
      restrict: "E",
      replace: true,
      scope: {
        pedidoSelected: "=", //pedido-selected
        onChangeStatusFunction: "=" //on-change-status
      },
      require: ['invertAppModal'],
      controller: function($scope){
        $scope.printing = false;
        $scope.printDiv = function printDiv(divName) {
          var printContents = document.getElementById(divName).innerHTML;
          var firmaclients = '<div class="form-group col-sm-6" style="width: 50%; display: inline-block;" >' +
            '<hr /> <br /> <center>Firma Cliente</center>'+
            '</div>';
          var firmadespacho = '<div class="form-group col-sm-6" style="width: 50%; display: inline-block;" >' +
            '<hr /> <br /> <center>Firma Despacho</center>'+
            '</div>';
          var popupWin = window.open('', '_blank', 'width=300,height=300');
          popupWin.document.open();
          popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" />' +
              '<style media="all" > .carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img {'+
          '}</style>'+
            '</head><body onload="window.print()">' + printContents +firmaclients+ firmadespacho+'</body></html>');
          popupWin.document.close();
        }
      },
      templateUrl: "/assets/template/pedidos/show-pedido-modal.html"
    }
  });
