angular.module('invertapp')
  .directive('createRolForm', [function (){
    return{
      restrict: "AE",
      require: ["invertAppTitle"],
      scope:{
        isSuccess: '=',       //is-success
        rolData: '=',        //pedido-data
        saveRolFunction: '=' //save-pedido-function
      },
      replace: true,
      templateUrl: "/public/dist/assets/template/roles/create-rol-form.html"
    }
  }]);