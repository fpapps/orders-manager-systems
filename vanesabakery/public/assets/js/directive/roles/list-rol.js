angular.module('invertapp')
  .directive('listRol', [function(){
    return{
      restrict: "E",
      require: ["invertAppTitle", 'searchBox', 'invertAppModal'],
      replace: true,
      scope: {
        currentPage: '=', //current-page
        getPageNumber: '=', //get-page-number
        header: '=',   //header
        onPageClick: '=', //on-page-click
        pages: '=',       //pages
        searchCriteria: '=',
        searchRol: '=', //search-rol
        sortByFunction: '=', //sort-by
        roles: '=', //roles,
        rolToBeDelete: '=' //rol-to-be-delete
      },
      controller: function($scope) {
        $scope.setRol = function setUser(rol) {
          $scope.rolToBeDelete = rol;
        }
      },
      templateUrl: "/public/dist/assets/template/roles/list-rol.html"
    }
  }]);

