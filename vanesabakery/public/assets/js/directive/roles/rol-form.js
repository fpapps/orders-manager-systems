angular.module('invertapp')
  .directive('rolForm', [function(){
    return{
      restrict: "E",
      require: ["invertAppTitle"],
      replace: true,
      scope: {
        isEdit: '=',
        isSuccess: "=",
        rolData: "=",
        saveRolFunction: '='
      },
      templateUrl: "/public/dist/assets/template/roles/rol-form.html"
    }
  }]);