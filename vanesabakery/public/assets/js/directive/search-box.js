angular.module('invertapp')
  .directive('searchBox', [function(){
    return{
      restrict: "E",
      scope: {
        onChange: '='
      },
      templateUrl: "/assets/template/general/search-box.html"
    }
  }]);
