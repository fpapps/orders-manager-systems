angular.module('invertapp')
  .directive('createUserForm', [function (){
    return{
      restrict: "AE",
      require: ["invertAppTitle"],
      scope:{
        isSuccess: '=',       //is-success
        userData: '=',        //user-data
        userRoles: '=',       //user-roles
        saveUserFunction: '=', //save-user-function,
        onChageFile: '=', //on-chage-file
        selectFileFunction: '=' //select-file-function
      },
      replace: true,
      templateUrl: "/assets/template/users/create-user-form.html"
    }
  }]);
