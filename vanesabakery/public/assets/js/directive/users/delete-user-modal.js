angular.module("invertapp")
  .directive('deleteUserModal', function(){
    return {
      restrict: "E",
      replace: true,
      scope: {
        userToBeDelete: "=",
        onRemoveFunction: '='
      },
      require: ['invertAppModal'],
      templateUrl: "/assets/template/users/delete-user-modal.html"
    }
  });
