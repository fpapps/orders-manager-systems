angular.module('invertapp')
  .directive('editUserForm', [function (){
    return{
      restrict: "AE",
      require: ["invertAppTitle"],
      scope:{
        isSuccess: '=',       //is-success
        isNotRequesting: '=', //is-not-requesting
        userData: '=',        //user-data
        userRoles: '=',       //user-roles
        saveUserFunction: '=' //save-user-function
      },
      replace: true,
      templateUrl: "/assets/template/users/edit-user-form.html"
    }
  }]);
