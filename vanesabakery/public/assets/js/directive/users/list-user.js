angular.module('invertapp')
  .directive('listUser', [function(){
    return{
      restrict: "E",
      require: ["invertAppTitle", 'searchBox', 'invertAppModal'],
      replace: true,
      scope: {
        currentPage: '=', //current-page
        getPageNumber: '=', //get-page-number
        header: '=',   //header
        onPageClick: '=', //on-page-click
        pages: '=',       //pages
        searchCriteria: '=',
        searchUser: '=', //search-user
        sortByFunction: '=', //sort-by
        users: '=', //users,
        userToBeDelete: '=' //user-to-be-delete
      },
      controller: function($scope) {
        $scope.setUser = function setUser(user) {
          $scope.userToBeDelete = user;
        }
      },
      templateUrl: "/assets/template/users/list-user.html"
    }
  }]);
