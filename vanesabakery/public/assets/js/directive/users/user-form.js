angular.module('invertapp')
  .directive('userForm', [function(){
    return{
      restrict: "E",
      require: ["invertAppTitle"],
      replace: true,
      scope: {
        isEdit: '=',
        isSuccess: "=",
        userData: "=",
        userRoles: '=',
        saveUserFunction: '=',
        onChageFile: '=',
        selectFileFunction: '='

      },

      templateUrl: "/assets/template/users/user-form.html"
    }
}]);
