angular.module("invertapp")
  .factory("$InvertAppAuthService", ["$InvertAppRequestService", function($InvertAppRequestService){
    var api = '/api/login';
    var loginUser = function loginUser(conf){
      conf.url = api;
      $InvertAppRequestService.httpPost(conf);
    };

    var getUser =function getUser(conf) {
      conf.url = '/api/authenticate';
      $InvertAppRequestService.httpGet(conf);
    };

    return {
      getUser: getUser,
      loginUser: loginUser
    };
  }]);