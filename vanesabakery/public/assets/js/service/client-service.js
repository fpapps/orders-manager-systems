angular.module("invertapp")
  .factory("$InvertAppClientService", ["$InvertAppRequestService", function ($InvertAppRequestService) {
    var api = '/api/clients';
    var createClient = function createClient(conf) {
      conf.url = api;
      conf.data.financiera_id = 1;
      $InvertAppRequestService.httpPost(conf);
    };

    var getClient = function (conf) {
      conf.url = api + '/' + conf.client_id;
      $InvertAppRequestService.httpGet(conf);
    };

    var getClients = function (conf) {
      conf.url = api;
      $InvertAppRequestService.httpGet(conf);
    };

    var getAllClients = function (conf) {
      conf.url = '/api/all_clients';
      $InvertAppRequestService.httpGet(conf);
    };

    var saveClient = function saveClient(conf) {
      conf.url = api + '/' + conf.data.id;
      conf.data.financiera_id = 1;
      $InvertAppRequestService.httpPatch(conf);
    };

    var searchClients = function searchClients(conf) {
      conf.url = api + '/' + 'search';
      $InvertAppRequestService.httpPost(conf);
    };

    var removeClient = function removeClient(conf) {
      conf.url = api + '/' + conf.data.id;
      $InvertAppRequestService.httpDelete(conf);
    };

    return {
      createClient: createClient,
      getClients: getClients,
      getClient: getClient,
      removeClient: removeClient,
      saveClient: saveClient,
      searchClients: searchClients,
      getAllClients: getAllClients
    };
  }]);
