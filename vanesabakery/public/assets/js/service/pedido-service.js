angular.module("invertapp")
  .factory("$InvertAppPedidoService", ["$InvertAppRequestService", function ($InvertAppRequestService) {
    var api = '/api/pedidos';
    var createPedido = function createPedido(conf) {
      conf.url = api;
      $InvertAppRequestService.httpPost(conf);
    };

    var getPedido = function (conf) {
      conf.url = api + '/' + conf.pedido_id;
      $InvertAppRequestService.httpGet(conf);
    };

    var getPedidos = function (conf) {
      conf.url = api;
      $InvertAppRequestService.httpGet(conf);
    };

    var savePedido = function saveUser(conf) {
      conf.url = api + '/' + conf.data.id;
      $InvertAppRequestService.httpPatch(conf);
    };

    var searchPedidos = function searchUsers(conf) {
      conf.url = '/api/pedido_by_status/'+conf.status;
      $InvertAppRequestService.httpGet(conf);
    };

    var removePedido = function removePedido(conf) {
      conf.url = api + '/' + conf.data.id;
      $InvertAppRequestService.httpDelete(conf);
    };
    
    var pedidoByStatus = function pedidoBySatus(conf){
      conf.url = '/api/pedido_by_status/'+conf.status;
      $InvertAppRequestService.httpGet(conf);      
    };

    return {
      createPedido: createPedido,
      getPedidos: getPedidos,
      getPedido: getPedido,
      removePedido: removePedido,
      savePedido: savePedido,
      searchPedidos: searchPedidos,
      pedidoByStatus: pedidoByStatus
    };
  }]);
