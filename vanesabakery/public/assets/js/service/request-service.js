angular.module('invertapp')
  .factory('$InvertAppRequestService', function($http){
    var defaultHeaders = {
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest'
    };

    var defaultDataType = 'JSON';

    var redirectToLogin= function redirectToLogin (){
      document.location.href = '/login';
    };

    var STATUS_REDIRECTS_CODE = {
      400: redirectToLogin,
      401: redirectToLogin
    };

    var httpRequest = function httpRequest(conf, method){
      if(conf.data){
        conf.data.token = localStorage.id_token;
      }
      $http({
        data: conf.data || { token : localStorage.id_token},
        dataType: conf.dataType || defaultDataType,
        headers: conf.headers || defaultHeaders,
        method: method,
        url: conf.url + "?token=" + localStorage.id_token
      }).then(function (result){
        conf.successCallback && conf.successCallback(result);
      }, function(error){
        if(document.location.href.indexOf('/login') === -1) {
          STATUS_REDIRECTS_CODE[error.status] && STATUS_REDIRECTS_CODE[error.status]();
        }
        conf.errorCallback && conf.errorCallback(error);
      });
    };

    var httpGet = function httpGet(conf){
      httpRequest(conf, "GET");
    };

    var httpPost = function httpGet(conf){
      httpRequest(conf, "POST");
    };

    var httpDelete = function httpGet(conf){
      httpRequest(conf, "DELETE");
    };

    var httpPatch = function httpGet(conf){
      httpRequest(conf, "PATCH");
    };

    var httpPut = function httpGet(conf){
      httpRequest(conf, "PUT");
    };

    return {
      httpGet: httpGet,
      httpPost: httpPost,
      httpDelete: httpDelete,
      httpPatch: httpPatch,
      httpPut: httpPut
    };
  });