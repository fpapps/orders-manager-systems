angular.module("invertapp")
  .factory("$InvertAppRolService", ["$InvertAppRequestService", function($InvertAppRequestService){
    var api = '/api/roles_all';
    var getRoles = function getRoles(conf){
      $InvertAppRequestService.httpGet({
        url: api,
        successCallback: conf.successCallback,
        errorCallback: conf.errorCallback
      });
    };

    var getPaginate = function getPaginate(conf){
      $InvertAppRequestService.httpGet({
        url: '/api/roles',
        successCallback: conf.successCallback,
        errorCallback: conf.errorCallback
      });
    };

    var searchRoles = function searchRoles(conf) {
      conf.url = '/api/roles/search';
      $InvertAppRequestService.httpPost(conf);
    };

    var createRol = function createUser(conf) {
      conf.url = '/api/roles';
      conf.data.financiera_id = 1;
      $InvertAppRequestService.httpPost(conf);
    };

    return {
      getRoles: getRoles,
      searchRoles: searchRoles,
      getPaginate: getPaginate,
      createRol: createRol
    };
  }]);
