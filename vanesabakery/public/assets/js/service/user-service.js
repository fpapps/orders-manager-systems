angular.module("invertapp")
  .factory("$InvertAppUserService", ["$InvertAppRequestService", function ($InvertAppRequestService) {
    var api = '/api/users';
    var createUser = function createUser(conf) {
      conf.url = api;
      conf.data.financiera_id = 1;
      $InvertAppRequestService.httpPost(conf);
    };

    var getUser = function (conf) {
      conf.url = api + '/' + conf.user_id;
      $InvertAppRequestService.httpGet(conf);
    };

    var getUsers = function (conf) {
      conf.url = api;
      $InvertAppRequestService.httpGet(conf);
    };

    var saveUser = function saveUser(conf) {
      conf.url = api + '/' + conf.data.id;
      conf.data.financiera_id = 1;
      $InvertAppRequestService.httpPatch(conf);
    };

    var searchUsers = function searchUsers(conf) {
      conf.url = api + '/' + 'search';
      $InvertAppRequestService.httpPost(conf);
    };

    var removeUser = function removeUser(conf) {
      conf.url = api + '/' + conf.data.id;
      $InvertAppRequestService.httpDelete(conf);
    };

    return {
      createUser: createUser,
      getUsers: getUsers,
      getUser: getUser,
      removeUser: removeUser,
      saveUser: saveUser,
      searchUsers: searchUsers
    };
  }]);
