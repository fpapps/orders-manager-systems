@extends('layouts.master')
@section('title', 'Crear Usuario')

@section('content')
    <section ng-controller="CreateClientController">
        <create-client-form
                is-success="isSuccess"
                client-data="clientData"
                save-client-function="createClient"
        ></create-client-form>
    </section>
@endsection

@push('scripts')
<script src="/assets/js/service/request-service.js" type="text/javascript"></script>
<script src="/assets/js/service/client-service.js" type="text/javascript"></script>
<script src="/assets/js/directive/general/invert-app-title.js" type="text/javascript"></script>
<script src="/assets/js/directive/clients/client-form.js" type="text/javascript"></script>
<script src="/assets/js/directive/clients/create-client-form.js" type="text/javascript"></script>
<script src="/assets/js/controller/clients/create.js" type="text/javascript"></script>
@endpush
