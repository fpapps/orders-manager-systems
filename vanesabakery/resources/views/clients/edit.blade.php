@extends('layouts.master')
@section('title', 'Editar Usuario')
@section('content')
    <section ng-controller="EditClientController">
        <edit-client-form
                is-success="isSuccess"
                is-not-requesting="!isRequestInProgress"
                client-data="clientData"
                save-client-function="saveClient"
        ></edit-client-form>
    </section>
@endsection

@push('scripts')
<script src="/assets/js/directive/general/invert-app-title.js" type="text/javascript"></script>
<script src="/assets/js/directive/clients/client-form.js" type="text/javascript"></script>
<script src="/assets/js/directive/clients/edit-client-form.js" type="text/javascript"></script>
<script src="/assets/js/service/request-service.js" type="text/javascript"></script>
<script src="/assets/js/service/client-service.js" type="text/javascript"></script>
<script src="/assets/js/service/rol-service.js" type="text/javascript"></script>
<script src="/assets/js/controller/clients/edit.js" type="text/javascript"></script>
@endpush