@extends('layouts.master')
@section('title', 'Listado Clientes')

@section('content')
    <section ng-controller="ListClientController">
        <list-client
                current-page="currentPage"
                client-to-be-delete="clientToBeDelete"
                get-page-number="getPageNumber"
                header="header"
                on-page-click="setPage"
                pages="pages"
                search-client="searchClient"
                sort-by-function="sortBy"
                clients="clients"
        ></list-client>
        <delete-client-modal
                client-to-be-delete="clientToBeDelete"
                on-remove-function="deleteClient"
        >
        </delete-client-modal>
    </section>
@endsection

@push('scripts')
<script src="/assets/js/directive/general/invert-app-title.js" type="text/javascript"></script>
<script src="/assets/js/service/request-service.js" type="text/javascript"></script>
<script src="/assets/js/service/client-service.js" type="text/javascript"></script>
<script src="/assets/js/directive/clients/list-client.js" type="text/javascript"></script>
<script src="/assets/js/directive/search-box.js" type="text/javascript"></script>
<script src="/assets/js/directive/modal.js" type="text/javascript"></script>
<script src="/assets/js/directive/clients/delete-client-modal.js" type="text/javascript"></script>
<script src="/assets/js/controller/clients/list.js" type="text/javascript"></script>
@endpush