@extends('layouts.master')
@section('title', 'Dashboard')

@section('content')
    <div ng-controller="DashboardController">
        <dashboard user="userData" ></dashboard>
    </div>
@endsection

@push('scripts')
<script src="/assets/js/controller/dashboard/dashboard-controller.js"></script>
<script src="/assets/js/directive/dashboard-directive.js"></script>
<script src="/assets/js/directive/general/invert-app-title.js"></script>
<script src="/assets/js/service/request-service.js"></script>
<script src="/assets/js/service/auth-service.js"></script>
@endpush
