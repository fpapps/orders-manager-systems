<div class="form-group">
    <label>{{ $label }}</label>
    <input type="password" name="{{ $name  }}"
           @if ($required)
               required
           @endif
           class="form-control" value="{{ $value  }}" />
</div>