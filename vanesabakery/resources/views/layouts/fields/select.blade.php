<div class="form-group">
    <label>{{ $label }}</label>
    <select name="{{ $name  }}" class="form-control" >
        @foreach($options as $option)
            <option
                    @if ($option['id'] == $selected)
                        selected
                    @endif
                    value="{{ $option['id'] }}" >{{ $option['label'] }}</option>
        @endforeach
    </select>
</div>