<div class="form-group">
    <label>{{ $label }}</label>
    <input type="text" name="{{ $name  }}"
           @if ($required)
               required
           @endif
           class="form-control" value="{{ $value  }}" />
</div>