<!DOCTYPE html>
<html lang="es" ng-app="invertapp">

@include('layouts.head')

    <body ng-cloak>
	<div id="wrapper">
        @section('sidebar')
            @include('layouts.menu')
        @show

        <div id="page-wrapper" >
            @yield('content')
        </div>
	</div>
    @include('layouts.scripts')
    @stack('scripts')

</body>

</html>