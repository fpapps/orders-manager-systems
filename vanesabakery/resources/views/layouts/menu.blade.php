<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation"  style="margin-bottom: 0" ng-controller="MenuController" >
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Vanessa Bakery</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <logout></logout>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <li  >
                    <a ><i class="fa fa-user fa-fw"></i> Usuarios<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li ng-if="auth.rol_id == 1" >
                            <a href="/users" >Listar</a>
                        </li>
                        <li ng-if="auth.rol_id == 1" >
                            <a href="/users/create" >Agregar</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li  >
                    <a  ><i class="fa fa-users fa-fw"></i> Clientes<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li ng-if="auth.rol_id == 1 || auth.rol_id == 2" >
                            <a href="/clients" >Listar</a>
                        </li>
                        <li ng-if="auth.rol_id == 1 || auth.rol_id == 2" >
                            <a href="/clients/create" >Agregar</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li >
                    <a ><i class="fa fa-birthday-cake fa-fw"></i> Pedidos<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="/pedidos" >Listar</a>
                        </li>
                        <li ng-if="auth.rol_id == 1 || auth.rol_id == 2" >
                            <a href="/pedidos/create" >Agregar</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>

@push('scripts')
<script src="/assets/js/controller/menu/menu.js" type="text/javascript"></script>
<script src="/assets/js/service/request-service.js" type="text/javascript"></script>
<script src="/assets/js/service/auth-service.js" type="text/javascript"></script>
@endpush