<?php
/**
 * Created by PhpStorm.
 * User: Carlos
 * Date: 17/7/2016
 * Time: 11:37 AM
 */
$build = App::environment() === "production" ? '.min' : '';
?>
<!-- jQuery -->
<script src="/assets/admin/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/assets/admin/bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="/assets/admin/dist/js/sb-admin-2.js"></script>
<!--Angular JS-->
<script src="/assets/js/third-party/angular/angular{{$build}}.js" ></script>
<script type="text/javascript">
    angular.module('invertapp', []);
</script>
<script src="/assets/js/directive/logout.js" type="text/javascript"></script>