@section('title', 'Inicio de Sesión')
<!DOCTYPE html>
<html lang="es" ng-app="invertapp">
@include('layouts.head')

<body ng-cloak>
    <!-- #wrapper -->
    <div id="wrapper">
        <div class="container">
            <login></login>
        </div>
    </div>
    <!-- /#wrapper -->

    @include('layouts.scripts')
    <script src="/assets/js/directive/login-directive.js" type="text/javascript"></script>
    <script src="/assets/js/service/request-service.js" type="text/javascript"></script>
    <script src="/assets/js/service/auth-service.js" type="text/javascript"></script>
    <script src="/assets/js/service/user-service.js" type="text/javascript"></script>
</body>

</html>

