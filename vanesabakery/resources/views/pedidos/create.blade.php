@extends('layouts.master')
@section('title', 'Crear Pedido')

@section('content')
    <section ng-controller="CreatePedidoController">
        <create-pedido-form
                is-success="isSuccess"
                pedido-data="pedidoData"
                pedido-clients="pedidoClients"
                save-pedido-function="createPedido"
        ></create-pedido-form>
    </section>
@endsection

@push('scripts')
<script src="/assets/js/third-party/moment.min.js" type="text/javascript"></script>
<script src="/assets/js/third-party/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script src="/assets/js/service/request-service.js" type="text/javascript"></script>
<script src="/assets/js/service/pedido-service.js" type="text/javascript"></script>
<script src="/assets/js/service/client-service.js" type="text/javascript"></script>
<script src="/assets/js/directive/general/invert-app-title.js" type="text/javascript"></script>
<script src="/assets/js/directive/general/myFile.js" type="text/javascript"></script>
<script src="/assets/js/directive/pedidos/pedido-form.js" type="text/javascript"></script>
<script src="/assets/js/directive/pedidos/create-pedido-form.js" type="text/javascript"></script>
<script src="/assets/js/controller/pedidos/create.js" type="text/javascript"></script>
<script src="/assets/js/jquery/datetime/datetime.js" type="text/javascript"></script>
@endpush
