@extends('layouts.master')
@section('title', 'Listada De Pedidos')

@section('content')
    <section ng-controller="ListPedidoController">

        <ul class="nav nav-tabs">
            <li role="presentation" ng-class="[{'active': tap.nuevo }]" ><a href="#" ng-click="filterByStatus('nuevo')">Nuevo</a></li>
            <li role="presentation" ng-class="[{'active': tap.preparando }]" ><a href="#" ng-click="filterByStatus('preparando')">Preparando</a></li>
            <li role="presentation" ng-class="[{'active': tap.listo }]" ><a href="#" ng-click="filterByStatus('listo')">Listo</a></li>
            <li role="presentation" ng-class="[{'active': tap.entregado }]" ><a href="#" ng-click="filterByStatus('entregado')">Entregado</a></li>
            <li role="presentation" ng-class="[{'active': tap.cancelado }]" ><a href="#" ng-click="filterByStatus('cancelado')">Cancelado</a></li>
        </ul>
        <list-pedido
                current-page="currentPage"
                get-page-number="getPageNumber"
                header="header"
                on-page-click="setPage"
                pages="pages"
                search-pedido="searchPedido"
                sort-by-function="sortBy"
                set-pedido="setPedido"
                pedidos="pedidos"
                pedido-selected="pedidoSelected",
                cancelar-pedido-function="cancelarPedido"
        ></list-pedido>
        <show-pedido-modal
                pedido-selected="pedidoSelected"
                on-change-status-function="savePedido"
        >
        </show-pedido-modal>
    </section>
@endsection

@push('scripts')
<script src="/assets/js/directive/general/invert-app-title.js" type="text/javascript"></script>
<script src="/assets/js/service/request-service.js" type="text/javascript"></script>
<script src="/assets/js/service/pedido-service.js" type="text/javascript"></script>
<script src="/assets/js/directive/pedidos/list-pedido.js" type="text/javascript"></script>
<script src="/assets/js/directive/search-box.js" type="text/javascript"></script>
<script src="/assets/js/directive/modal.js" type="text/javascript"></script>
<script src="/assets/js/directive/pedidos/show-pedido-modal.js" type="text/javascript"></script>
<script src="/assets/js/controller/pedidos/list.js" type="text/javascript"></script>
@endpush