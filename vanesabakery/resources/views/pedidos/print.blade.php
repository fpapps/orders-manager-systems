@extends('layouts.master')
@section('title', 'Editar Pedido')
@section('content')
    <section ng-controller="EditPedidoController">
        <edit-pedido-form
                is-success="isSuccess"
                is-not-requesting="!isRequestInProgress"
                pedido-data="pedidoData"
                pedido-clients="pedidoClients"
                save-pedido-function="savePedido"
        ></edit-pedido-form>
    </section>
@endsection

@push('scripts')
<script src="/assets/js/third-party/moment.min.js" type="text/javascript"></script>
<script src="/assets/js/third-party/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script src="/assets/js/directive/general/invert-app-title.js" type="text/javascript"></script>
<script src="/assets/js/directive/pedidos/pedido-form.js" type="text/javascript"></script>
<script src="/assets/js/directive/pedidos/edit-pedido-form.js" type="text/javascript"></script>
<script src="/assets/js/service/request-service.js" type="text/javascript"></script>
<script src="/assets/js/service/pedido-service.js" type="text/javascript"></script>
<script src="/assets/js/service/client-service.js" type="text/javascript"></script>
<script src="/assets/js/controller/pedidos/edit.js" type="text/javascript"></script>
@endpush
