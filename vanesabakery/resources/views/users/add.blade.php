@extends('layouts.master')

@section('title', 'Users')

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Agregando un nuevo usuario</h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Agregue su usuario
                </div>
                <div class="panel-body">
                    <div class="row">

                            <form id="user" role="form" name="user" >
                                @foreach($user->formFields($user) as $field)
                                    <div class="col-lg-6">
                                        @include(sprintf('layouts.fields.%s', $field['type']), $field)
                                    </div>
                                @endforeach

                                <div class="col-lg-12">
                                    <hr />
                                    <span id="save" class="btn btn-success pull-right"><i class="fa fa-save fa-fw"></i> Save</span>
                                </div>
                            </form>

                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.row -->
@endsection
<?php
        $data = [
            'update' => $update,
            'user' => $user
        ];
?>
<script>
    var gobla = <?php echo json_encode($data);?>;
</script>
@push('scripts')
    <script src="/assets/admin/controllers/user.js"></script>
    <script src="/assets/admin/extends/serialize.object.js"></script>
@endpush