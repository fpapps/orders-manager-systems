@extends('layouts.master')
@section('title', 'Crear Usuario')

@section('content')
    <section ng-controller="CreateUserController">
        <create-user-form
                is-success="isSuccess"
                user-data="userData"
                user-roles="userRoles"
                user-role-id-selected="userRoleIdSelected"
                save-user-function="createUser"
                on-chage-file="onChangeFoto"
                select-file-function="selectFoto"
        ></create-user-form>
    </section>
@endsection

@push('scripts')
<script src="/assets/js/service/request-service.js" type="text/javascript"></script>
<script src="/assets/js/service/user-service.js" type="text/javascript"></script>
<script src="/assets/js/service/rol-service.js" type="text/javascript"></script>
<script src="/assets/js/directive/general/invert-app-title.js" type="text/javascript"></script>
<script src="/assets/js/directive/users/user-form.js" type="text/javascript"></script>
<script src="/assets/js/directive/users/create-user-form.js" type="text/javascript"></script>
<script src="/assets/js/controller/users/create.js" type="text/javascript"></script>
@endpush
