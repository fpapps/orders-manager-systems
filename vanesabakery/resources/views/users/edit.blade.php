@extends('layouts.master')
@section('title', 'Editar Usuario')
@section('content')
    <section ng-controller="EditUserController">
        <edit-user-form
                is-success="isSuccess"
                is-not-requesting="!isRequestInProgress"
                user-data="userData"
                user-roles="userRoles"
                user-role-id-selected="userRoleIdSelected"
                save-user-function="saveUser"
        ></edit-user-form>
    </section>
@endsection

@push('scripts')
<script src="/assets/js/directive/general/invert-app-title.js" type="text/javascript"></script>
<script src="/assets/js/directive/users/user-form.js" type="text/javascript"></script>
<script src="/assets/js/directive/users/edit-user-form.js" type="text/javascript"></script>
<script src="/assets/js/service/request-service.js" type="text/javascript"></script>
<script src="/assets/js/service/user-service.js" type="text/javascript"></script>
<script src="/assets/js/service/rol-service.js" type="text/javascript"></script>
<script src="/assets/js/controller/users/edit.js" type="text/javascript"></script>
@endpush
