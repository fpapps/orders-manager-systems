@extends('layouts.master')
@section('title', 'Listado Usuarios')

@section('content')
    <section ng-controller="ListUserController">
        <list-user
                current-page="currentPage"
                user-to-be-delete="userToBeDelete"
                get-page-number="getPageNumber"
                header="header"
                on-page-click="setPage"
                pages="pages"
                search-user="searchUser"
                sort-by-function="sortBy"
                users="users"
        ></list-user>
        <delete-user-modal
                user-to-be-delete="userToBeDelete"
                on-remove-function="deleteUser"
        >
        </delete-user-modal>
    </section>
@endsection

@push('scripts')
<script src="/assets/js/directive/general/invert-app-title.js" type="text/javascript"></script>
<script src="/assets/js/service/request-service.js" type="text/javascript"></script>
<script src="/assets/js/service/user-service.js" type="text/javascript"></script>
<script src="/assets/js/directive/users/list-user.js" type="text/javascript"></script>
<script src="/assets/js/directive/search-box.js" type="text/javascript"></script>
<script src="/assets/js/directive/modal.js" type="text/javascript"></script>
<script src="/assets/js/directive/users/delete-user-modal.js" type="text/javascript"></script>
<script src="/assets/js/controller/users/list.js" type="text/javascript"></script>
@endpush