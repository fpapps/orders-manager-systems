@extends('layouts.master')
@section('title', 'Users')

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Usuarios del sistema </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Lista de usuarios
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">


                <div class="dataTable_wrapper table-responsive">
                            <a href="/users/add" class="btn btn-info pull-right margin-booton-18 " ><i class="fa fa-plus fa-fw"></i> Agregar Usuario</a>

                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Cedula</th>
                                    <th>Telefono</th>
                                    <th>Email</th>
                                    <th>Rol</th>
                                    <th >Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <td>{{ $user->nombre }}</td>
                                            <td>{{ $user->cedula }}</td>
                                            <td>{{ $user->telefono }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->rol->nombre }}</td>
                                            <td>
                                                <a href="/users/edit/{{$user->id}}" class="btn btn-primary"><i class="fa fa-pencil fa-fw"></i></a>
                                                <a href="/users/delete/{{$user->id}}" class="btn btn-danger" ><i class="fa fa-trash fa-fw"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                    {!! $users->render() !!}
                </div>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
