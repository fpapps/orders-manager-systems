<!DOCTYPE html>
<html lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Vanessa Bakery</title>
  <meta name="description" content="This is a form to manage the order of Vanessa Bakery">  


  <link rel="stylesheet" href="<?php echo url('assets/css/bootstrap.min.css'); ?>" />
</head>

<body>

 
<script data-main="<?php echo url('assets/js/app'); ?>" 
	src="<?php echo url('assets/js/node_modules/requirejs/require.js'); ?>" ></script>
</body>
</html>
