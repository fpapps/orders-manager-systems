<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Client;

class ClientTest extends TestCase
{
	use DatabaseMigrations;
	use WithoutMiddleware;
	
	protected $header = [ 
		'Content-Type' => 'application/x-www-form-urlencoded',
		'X-Requested-With' => 'XMLHttpRequest',
		'HTTP_X-Requested-With' => 'XMLHttpRequest'
	];
			
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
	
	public function testClients()
	{
		factory(Client::class,8)->create();
		$client = Client::find(1);
		$this->get( 'api/clients',  $this->header)
		->seeJson([ 
		'id' => $client->id 
		]);
	}


	public function testCreateClient()
	{
		$client = factory(Client::class)->make()->toArray();
		$this->post('api/clients', $client, $this->header)
		->seeJson([
                 'message' => 'Client saved',
             ]);
	}

	public function testUpdateClient()
	{
		$client = factory(Client::class)->create()->toArray();
		$client['nombre'] = 'Juan';
		
		$this->patch('api/clients/'.$client['id'], $client, $this->header)
             ->seeJson([
                 'message' => 'Client updated',
             ]);
        $this->assertEquals($client['nombre'], Client::find($client['id'])->nombre);

	}
	
	public function testClient()
	{
		$client = factory(Client::class)->create();
		$this->get( 'api/clients/'.$client->id,  $this->header)
		->seeJson([ 
		'id' => $client->id 
		]);
		
	}

	public function testDeleteClient()
	{
		$client = factory(Client::class)->create();
		$this->delete( 'api/clients/'.$client->id)
             ->seeJson([
                 'message' => 'Client eliminado'
             ]);

	}
}
