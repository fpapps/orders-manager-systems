<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Rol;

class RolTest extends TestCase
{
	use DatabaseMigrations;
	use WithoutMiddleware;
	
	protected $header = [ 
			'Content-Type' => 'application/x-www-form-urlencoded',
        	'X-Requested-With' => 'XMLHttpRequest',
        	'HTTP_X-Requested-With' => 'XMLHttpRequest'
        	];
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
	
	public function testCreateRol()
	{
		$rol = factory(Rol::class)->make()->toArray();
		
		//dd($rol);
		$this->post('api/roles', $rol, $this->header)
		->seeJson([
                 'message' => 'Rol saved',
             ]);
	}
	
	public function testRoles()
	{
		factory(Rol::class,8)->create();
		$rol = Rol::find(1);
		$this->get( 'api/roles',  $this->header)
		->seeJson([ 
		'id' => $rol->id 
		]);
	}
	
	public function testUpdateRol()
	{
		
		$rol = factory(Rol::class)->create();
		$rol->nombre = "Servicio al cliente";
		$this->patch( 'api/roles/'.$rol->id, $rol->toArray(), $this->header)
             ->seeJson([
                 'message' => 'Rol updated',
             ]);
        $this->assertEquals($rol->nombre, Rol::find($rol->id)->nombre);

	}
	
	public function testRol()
	{
		$rol = factory(Rol::class)->create();
		$this->get( 'api/roles/'.$rol->id,  $this->header)
		->seeJson([ 
		'id' => $rol->id 
		]);
		
	}

	public function testDeleteRol()
	{
		$rol = factory(Rol::class)->create();
		$this->delete( 'api/roles/'.$rol->id)
             ->seeJson([
                 'message' => 'Rol eliminado'
             ]);

	}
}
