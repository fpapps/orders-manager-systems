<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class UserTest extends TestCase
{
	use DatabaseMigrations;
	use WithoutMiddleware;
	
	protected $header = [ 
			'Content-Type' => 'application/x-www-form-urlencoded',
        	'X-Requested-With' => 'XMLHttpRequest',
        	'HTTP_X-Requested-With' => 'XMLHttpRequest'
        	];
			
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
	
	public function testUsers()
	{
		factory(User::class,8)->create();
		$user = User::find(1);
		$this->get( 'api/users',  $this->header)
		->seeJson([ 
		'id' => $user->id 
		]);
	}


	public function testCreateUser()
	{
		$user = factory(User::class)->make()->toArray();
		$user['password'] = '12345678';
		$user['password_confirmation'] = '12345678';
		
		$this->post('api/users', $user, $this->header)
		->seeJson([
                 'message' => 'User saved',
             ]);
	}

	public function testUpdateUser()
	{
		
		$user = factory(User::class)->create();
		$user->nombre = "Julio";
		$this->patch( 'api/users/'.$user->id, $user->toArray(), $this->header)
             ->seeJson([
                 'message' => 'User updated',
             ]);
        $this->assertEquals($user->nombre, User::find($user->id)->nombre);

	}
	
	public function testUser()
	{
		$user = factory(User::class)->create();
		$this->get( 'api/users/'.$user->id,  $this->header)
		->seeJson([ 
		'id' => $user->id 
		]);
		
	}

	public function testDeleteUser()
	{
		$user = factory(User::class)->create();
		$this->delete( 'api/users/'.$user->id)
             ->seeJson([
                 'message' => 'Usuario eliminado'
             ]);

	}
}
